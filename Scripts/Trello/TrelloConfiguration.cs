﻿using System.Collections;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TrelloConfiguration : TrackerConfiguration 
{
    [Header("Trello Auth")]
    [SerializeField]
    private string _key;
    [SerializeField]
    private string _token;

    [Header("Trello Settings")]
    [SerializeField]
    private string _defaultBoard;
    [SerializeField]
    private string _defaultList;
    [SerializeField]

    public string key {
        set => _key = value;
    }

    public string token {
        set => _token = value;
    }

    /// <summary>
    /// Sends a given Trello card using the authorization settings.
    /// </summary>
    /// <param name="card">Trello card to send.</param>
    /// <param name="list">Overrides default list.</param>
    /// <param name="board">Overrides default board.</param>
    /// <param name="label">Overrides default label.</param>
    public void SendNewCard(TrelloCard card, string list = null, string board = null, string label = null)
    {
        bugReportInProgress = true;
        if (board == null) {
            board = _defaultBoard;
        }

        if (list == null) {
            list = _defaultList;
        }
        StartCoroutine(Send_Internal(card, list, board, label));
    }

    private IEnumerator Send_Internal(TrelloCard card, string list, string board, string label) {

        // Create an API instance
        var api = new TrelloAPI(_key, _token);

        // Wait for the Trello boards
        yield return api.PopulateBoards();
        api.SetCurrentBoard(board);

        // Wait for the Trello lists
        yield return api.PopulateLists();
        api.SetCurrentList(list);

        // Set the current ID of the selected list
        card.idList = api.GetCurrentListId();
        // Upload to the server
        yield return api.UploadCard(card);

        bugReportInProgress = false;
    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(TrelloConfiguration))]
public class TrelloSendInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.Space(20);
        
        if (GUILayout.Button("Trello Authorization Key"))
        {
            Application.OpenURL("https://trello.com/app-key");
        }
        GUILayout.Label("First time Trello setup");
        GUILayout.Label("1. Log into Trello and click the Authorization Key above");
        GUILayout.Label("2. Copy the key listed here into the Key field listed above");
        GUILayout.Label("3. Click the 'Token' hyperlink to get to the Token screen");
        GUILayout.Label("4. Click Allow to get a page that will display a token for you.");
        GUILayout.Label("5. Copy the token here into the Token field listed above.");
    }
}
#endif