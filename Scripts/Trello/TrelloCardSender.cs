﻿using System;
using System.Collections;
using System.Text;
using UnityEngine;

namespace MRC.IssueTracker
{
    public class TrelloCardSender : CardSender
    {
        public TrelloConfiguration trelloSend;
        
        /// <summary>
        /// Send a Trello Card using the info provided on the UI.
        /// </summary>
        public override void SendCardToBoard(string title, string description)
        {
            StartCoroutine(SendCard_Internal(title, description));
        }

        private IEnumerator SendCard_Internal(string title, string description)
        {
            TrelloCard card = new TrelloCard();
            card.name = title;
            card.desc = description;
            card.desc = "#" + Application.productName + " version: " + Application.version + "\n" +
                        (Application.isEditor ? "**This was logged from the Unity Editor**\n" : "\n") +
                        "___\n" +
                        "###Description\n" +
                        description + "\n" +
                        "_Submitted by: " + SystemInfo.deviceName + "_\n" +
                        "___\n" +
                        GetSystemInformation() +
                        "###Gameplay Information\n" +
                        GetTotalSessionPlaytime() +
                        GetScenePlaytime();
            if (playerLogger.loggingEnabled){
                card.desc +=
                "###Player Log\n" +
                playerLogger.logData;
            }
            card.screenshotSource = bugReportUI.file;
            card.screenshotName = DateTime.UtcNow.ToString() + ".jpg";
            
            // Store playerLogger.logData in binary
            byte[] bytes = Encoding.ASCII.GetBytes(playerLogger.logData);
            card.logSource = bytes;
            card.logName = "playerData.txt";
            
            trelloSend.SendNewCard(card);
            while (trelloSend.bugReportInProgress)
            {
                yield return new WaitForEndOfFrame();
            }
            bugReportUI.Hide();
        }
    }
}