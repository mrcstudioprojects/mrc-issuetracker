﻿/*
 * TrelloCard.cs
 * Base class for a Trello card.
 * 
 * Original by bfollington
 * https://github.com/bfollington/Trello-Cards-Unity
 * 
 * by Àdam Carballo under MIT license.
 * https://github.com/AdamCarballo/Unity-Trello
 * 
 * by Peter Linder under MIT License
 */

public class TrelloCard : TrackerCard {
	
    public string due = "null";
    public string urlSource = "null";
    public byte[] screenshotSource = null;
    public string screenshotName = null;
    public string idList = ""; 
    public byte[] logSource = null;
    public string logName = null;
    
    /// <summary>
    /// Base class for a Trello card.
    /// </summary>
    public TrelloCard() {
			
    }		
}