﻿/*
 * TrelloAPI.cs
 * Interact directly with the Trello API using MiniJSON and uploads cards. 
 * 
 * Original by bfollington
 * https://github.com/bfollington/Trello-Cards-Unity
 * 
 * by Àdam Carballo under MIT license.
 * https://github.com/AdamCarballo/Unity-Trello
 */

using System.Collections.Generic;
using UnityEngine;
using MiniJSON;
using UnityEngine.Networking;

public class TrelloAPI {

    // Base URLs for Trello objects
    private const string MemberBaseUrl = "https://api.trello.com/1/members/me";
	private const string BoardBaseUrl = "https://api.trello.com/1/boards/";
    private const string ListBaseUrl = "https://api.trello.com/1/lists/";
	private const string CardBaseUrl = "https://api.trello.com/1/cards/";
    private const string LabelBaseUrl = "https://api.trello.com/1/labels/";

    private readonly string _token;
	private readonly string _key;
		
    // Collections to store retrieved objects
	private List<object> _boards;
	private List<object> _lists;
    private List<object> _labels;

    private string _currentBoardId;
	private string _currentListId;
    private string _currentLabelId;


    /// <summary>
    /// Generate new Trello API instance.
    /// </summary>
    /// <param name="key">Trello API key, keep it private.</param>
    /// <param name="token">Trello API token, keep it private.</param>
    public TrelloAPI(string key, string token) {

		_key = key;
		_token = token;
	}
		
	/// <summary>
	/// Checks if a UnityWebRequest object returned an error, and if so throws an exception.
	/// </summary>
	/// <param name="errorMessage">Error message to display.</param>
	/// <param name="uwr">The UnityWebRequest object.</param>
	private void CheckWebRequestStatus(string errorMessage, UnityWebRequest uwr) {
		switch (uwr.result) {
			case UnityWebRequest.Result.ConnectionError:
			case UnityWebRequest.Result.ProtocolError:
			case UnityWebRequest.Result.DataProcessingError:
				throw new TrelloException($"{errorMessage}: {uwr.error} ({uwr.downloadHandler.text})");
		}
	}

    #region Board Functions
    /// <summary>
    /// Download the list of available boards for the user and store them.
    /// </summary>
    /// <returns>Downloaded boards.</returns>
    public List<object> PopulateBoards() {
		_boards = null;
		var uwr = UnityWebRequest.Get($"{MemberBaseUrl}?key={_key}&token={_token}&boards=all");
		var operation = uwr.SendWebRequest();

		// Wait for request to return
		while (!operation.isDone) {
			CheckWebRequestStatus("The Trello servers did not respond.", uwr);
		}

		if (!(Json.Deserialize(uwr.downloadHandler.text) is Dictionary<string, object> boardsDict)) {
			throw new TrelloException("No boards found or something went wrong.");
		}
			
		_boards = (List<object>) boardsDict["boards"];
		return _boards;
	}
		
	/// <summary>
	/// Sets the given board to search for lists in.
	/// </summary>
	/// <param name="name">Name of the board.</param>
	public void SetCurrentBoard(string name) {

		if (_boards == null) {
			throw new TrelloException("There are no boards available. Either the user does not have access to a board or PopulateBoards() wasn't called.");
		}
			
		for (var i = 0; i < _boards.Count; i++) {
			var board = (Dictionary<string, object>) _boards[i];
			if ((string)board["name"] != name) continue;
				
			_currentBoardId = (string) board["id"];
			return;
		}
			
		_currentBoardId = null;
		throw new TrelloException($"A board with the name {name} was not found.");
	}
    #endregion Board Functions

    #region List Functions

    /// <summary>
    /// Download all the lists of the selected board and store them.
    /// </summary>
    /// <returns>Downloaded list.</returns>
    public List<object> PopulateLists()
    {

        _lists = null;

        if (_currentBoardId == null)
        {
            throw new TrelloException("Cannot retrieve the lists, there isn't a selected board yet.");
        }

        var uwr = UnityWebRequest.Get($"{BoardBaseUrl}{_currentBoardId}?key={_key}&token={_token}&lists=all");
        var operation = uwr.SendWebRequest();

        // Wait for request to return
        while (!operation.isDone)
        {
            CheckWebRequestStatus("Connection to the Trello servers was not possible.", uwr);
        }

        if (!(Json.Deserialize(uwr.downloadHandler.text) is Dictionary<string, object> listsDict))
        {
            throw new TrelloException("No lists found or something went wrong.");
        }

        _lists = (List<object>)listsDict["lists"];
        return _lists;
    }


    /// <summary>
    /// Sets the given list to upload cards to.
    /// </summary>
    /// <param name="name">Name of the list.</param>
    public void SetCurrentList(string name)
    {

        if (_lists == null)
        {
            throw new TrelloException("There are no lists available. Either the board does not contain lists or PopulateLists() wasn't called.");
        }

        for (var i = 0; i < _lists.Count; i++)
        {
            var list = (Dictionary<string, object>)_lists[i];
            if ((string)list["name"] != name) continue;

            _currentListId = (string)list["id"];
            return;
        }

        // If no List is found with that name, create it.
        CreateNewList(name);
    }

    /// <summary>
    /// Generates a new Trello List.
    /// 
    /// On a successful http response:
    /// Repopulates stored lists, then sets the newly created list as the current list, via their respective functions.
    /// 
    /// <param name="listName">The Name of a list to create.</param>
    /// <see cref="PopulateLists()"/>
    /// <see cref="SetCurrentList(string)"/>
    /// </summary>
    public void CreateNewList(string listName)
    {
        var post = new WWWForm();
        post.AddField("name", listName);
        post.AddField("idBoard", _currentBoardId);

        var uwr = UnityWebRequest.Post($"{ListBaseUrl}?key={_key}&token={_token}", post);
        var operation = uwr.SendWebRequest();

        // Wait for request to return
        while (!operation.isDone)
        {
            CheckWebRequestStatus("Could not generate a new list.", uwr);
        }

        Debug.Log($"Trello list created!\nResponse {uwr.responseCode}\nName: {listName}\nOn board with ID of idBoard: {_currentBoardId}");
            
        // If the Request to create a board was successful, update lists
        if (uwr.responseCode == 200)
        {
            PopulateLists();
            SetCurrentList(listName);
        }
    }

    /// <summary>
    /// Returns the selected Trello list id.
    /// </summary>
    /// <returns>The list id.</returns>
    public string GetCurrentListId() {

		if (_currentListId == null) {
			throw new TrelloException("A list has not been selected. Call SetCurrentList() first.");
		}
		return _currentListId;
	}
    #endregion List Functions

    #region Label Functions

    /// <summary>
    /// Download all the labels of the selected board and store them.
    /// </summary>
    /// <returns>Downloaded labels.</returns>
    public List<object> PopulateLabels()
    {

        _labels = null;

        if (_currentBoardId == null)
        {
            throw new TrelloException("Cannot retrieve the labels, there isn't a selected board yet.");
        }

        var uwr = UnityWebRequest.Get($"{BoardBaseUrl}{_currentBoardId}?key={_key}&token={_token}&labels=all");
        var operation = uwr.SendWebRequest();

        // Wait for request to return
        while (!operation.isDone)
        {
            CheckWebRequestStatus("Connection to the Trello servers was not possible.", uwr);
        }

        if (!(Json.Deserialize(uwr.downloadHandler.text) is Dictionary<string, object> listsDict))
        {
            throw new TrelloException("No labels found or something went wrong.");
        }

        _labels = (List<object>)listsDict["labels"];
        return _labels;

    }

    /// <summary>
    /// Sets the label to add to a card.
    /// This assumes that there will be only 1 label applied to a card (Selected via slider, as per client reqs)
    /// </summary>
    /// <param name="labelName">Name of the label.</param>
    public void SetCurrentLabel(string labelName)
    {
        // TO-DO:: Alter search to find all 4 labels (flags?)
        // Send request to make those 4 labels, if not existing

        if (_labels == null)
        {
            throw new TrelloException("There are no labels available. Either the board does not contain labels or PopulateLabels() wasn't called.");
        }

        for (var i = 0; i < _labels.Count; i++)
        {
            var list = (Dictionary<string, object>)_labels[i];
            if ((string)list["name"] != labelName) continue;

            _currentLabelId = (string)list["id"];
            return;
        }

        // If no label is found with that name, create it.
        CreateNewLabel(labelName);
    }

    /// <summary>
    /// Generates a new Trello Label.
    /// Refer to https://developer.atlassian.com/cloud/trello/rest/api-group-labels/#api-labels-post
    /// 
    /// On a successful http response:
    /// Repopulates stored labels, then uses the newly created label, via its respective function.
    /// 
    /// <param name="labelName">The Name of a list to create.</param>
    /// <see cref="PopulateLabels()"/>
    /// <see cref="SetCurrentLabel(string)"/>
    /// </summary>
    public void CreateNewLabel(string labelName)
    {
        string labelColor = "black";  // American spelling keeps tripping me up

        switch (labelName)
        {
            case "Feedback":
                // labelColor = "black";
                break;
            case "Low Priority":
                labelColor = "green";
                break;
            case "Medium Priority":
                labelColor = "yellow";
                break;
            case "High Priority":
                labelColor = "red";
                break;
        }
        
        var post = new WWWForm();
        post.AddField("name", labelName);
        post.AddField("color", labelColor);
        post.AddField("idBoard", _currentBoardId);

        var uwr = UnityWebRequest.Post($"{LabelBaseUrl}?key={_key}&token={_token}", post);
        var operation = uwr.SendWebRequest();

        // Wait for request to return
        while (!operation.isDone)
        {
            CheckWebRequestStatus("Could not generate a new label.", uwr);
        }

        Debug.Log($"Trello label created!\nResponse {uwr.responseCode}\nName: {labelName}\nColor: {labelColor}\nOn board with ID of idBoard: {_currentBoardId}");

        // If the Request to create a label was successful, update labels
        if (uwr.responseCode == 200)
        {
            PopulateLabels();
            SetCurrentLabel(labelName);
        }
    }

    /// <summary>
    /// Returns the selected Trello label id.
    /// </summary>
    /// <returns>The list id.</returns>
    public string GetCurrentLabelId()
    {

        if (_currentLabelId == null)
        {
            throw new TrelloException("A label has not been selected. Call SetCurrentList() first.");
        }
        return _currentLabelId;
    }

    #endregion Label Functions

    /*/// <summary>
	/// Given an exception object, a TrelloCard is created and populated with the relevant information from the exception. This is then uploaded to the Trello server.
	/// </summary>
	/// <returns>The exception card.</returns>
	/// <param name="e">E.</param>
	public TrelloCard uploadExceptionCardd(Exception e) {

		TrelloCard card = new TrelloCard();
		card.name = e.GetType().ToString();
		card.due = DateTime.Now.ToString();
		card.desc = e.Message;
		card.idList = _currentListId;
			
		return UploadCard(card);
	}*/

    /// <summary>
    /// Uploads a given TrelloCard object to the Trello server.
    /// </summary>
    /// <returns>Trello card uploaded.</returns>
    /// <param name="card">Trello card to upload.</param>
    public TrelloCard UploadCard(TrelloCard card) {

	    var post = new WWWForm();
	    post.AddField("name", card.name);
	    post.AddField("desc", card.desc);
	    post.AddField("due", card.due);
	    post.AddField("idList", card.idList);
	    post.AddField("urlSource", card.urlSource);

	    if (card.screenshotSource != null && card.screenshotName != null) {
		    post.AddBinaryData("fileSource", card.screenshotSource, card.screenshotName);
	    }

	    var uwr = UnityWebRequest.Post($"{CardBaseUrl}?key={_key}&token={_token}", post);
	    var operation = uwr.SendWebRequest();

	    Debug.Log("SENT\n" + post.headers);

	    // Wait for request to return
	    while (!operation.isDone) {
		    CheckWebRequestStatus("Could not upload the Trello card.", uwr);
	    }

	    Debug.Log($"Trello card sent!\nResponse {uwr.responseCode}\nName: {card.name}\nDescription: {card.desc}\nDue Date: {card.due}\nidList: {card.idList}\nurlSource: {card.urlSource}");
	    Debug.Log("SENT\n" + post.headers);
        
	    // Send Log
	    post = new WWWForm();
	    post.AddField("name", card.logName);
	    post.AddField("mimeType", "txt");

	    if (card.logSource != null && card.logName != null) {
		    post.AddBinaryData("file", card.logSource, card.logName);
	    }

	    // Get ID from recently created card
	    if (!(Json.Deserialize(uwr.downloadHandler.text) is Dictionary<string, object> listsDict)) {
		    throw new TrelloException("Unable to parse response data from created card.");
	    }

	    uwr = UnityWebRequest.Post($"{CardBaseUrl}/{listsDict["id"]}/attachments?key={_key}&token={_token}", post);
	    operation = uwr.SendWebRequest();

	    // Wait for request to return
	    while (!operation.isDone) {
		    CheckWebRequestStatus("Could not upload the Trello attachments", uwr);
	    }
      
	    return card;
	}
}