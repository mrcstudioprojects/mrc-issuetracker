using System;
using UnityEngine;

public class PlayerLogger : MonoBehaviour
{
    public bool loggingEnabled = false;
    
    void OnEnable() 
    {
        if (loggingEnabled)
        {
            Application.logMessageReceived += AddToLog;
        }
    }

    void OnDisable()
    {
        if (loggingEnabled)
        {
            Application.logMessageReceived -= AddToLog;
        }
    }

    [Multiline]
    public string logData;
    
    void AddToLog(string a, string b, LogType t)
    {
        string errorType ="";
        if (t == LogType.Log)
        {
            errorType = "📝️";
        }
        else if (t == LogType.Assert)
        {
            errorType = "📌";
        }
        else if (t == LogType.Error)
        {
            errorType = "🛑";
        }
        else if (t == LogType.Exception)
        {
            errorType = "⛔️";
        }
        else if (t == LogType.Warning)
        {
            errorType = "⚠️";
        }
        logData = logData + Environment.NewLine + " " + errorType + " " +  System.DateTime.Now + " " + a + " - " + b;
    }
}
