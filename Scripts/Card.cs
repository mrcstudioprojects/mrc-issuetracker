using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Part of a refactor, the Card class will contain the information that is shared between all APIs, basically
/// a storage container for information. This is meant to replace the card classes defined for each of the
/// different platforms, as they largely use the same information and having multiple different classes
/// is redundant.
/// </summary>
namespace MRC.IssueTracker
{
    public class Card
    {
        public string name;
        public string description;
        public Vector3 cameraLocation;
        public string imageLocation;
        public bool hasImage;

        //Non parameterized constructor will have default values, no name, image or description
        public Card()
        {
            name = "No name provided";
            description = "<body>User Description: " +
                          "No description provided by user" + SystemInfoAndTime();
            cameraLocation = Camera.main.transform.position;

            description += SystemInfoAndTime();
        }

        //Parameterized constructor without image
        public Card(string _name, string _description)
        {
            name = _name;
            description = "<body>User Description: " +
                          _description + SystemInfoAndTime();
            hasImage = false;
            cameraLocation = Camera.main.transform.position;

        }

        //Parameterized constructor with image
        public Card(string _name, string _description, string _imageLocation)
        {
            name = _name;
            description = "<body>User Description: " +
                          _description + SystemInfoAndTime();
            imageLocation = _imageLocation;
            hasImage = true;
            cameraLocation = Camera.main.transform.position;

        }

        private string SystemInfoAndTime()
        {
            return "<ul><li>Demo " + Application.version + "</li>" +
                   "<li>System Information</li>" +
                   "<li>" + SystemInfo.operatingSystem + "</li>" +
                   "<li>" + SystemInfo.processorType + "</li>" +
                   "<li>" + SystemInfo.systemMemorySize + "</li>" +
                   "<li>" + SystemInfo.graphicsDeviceName + " (" + SystemInfo.graphicsDeviceType + ")" + "</li>" +
                   "<li>" + "Resolution: " + Screen.width + "x" + Screen.height + "</li>" +
                   "<li>Other Information</li>" +
                   "<li>Playtime: " + String.Format("{0:0}:{1:00}", Mathf.Floor(Time.time / 60), Time.time % 60) +
            "Tracked object position: " + cameraLocation + "</li></ul></body>";
        }

        public override string ToString()
        {
            return $"{name}newline{description}";
        }
    }
}
