using System;
using System.Text;

/// <summary>
/// @see: https://stackoverflow.com/questions/11743160/how-do-i-encode-and-decode-a-base64-string
/// </summary>
///

namespace MRC.IssueTracker
{
    public static class Base64
    {
        /// <summary>
        /// Converts a string to Base64
        /// </summary>
        /// <param name="text">Any string to be Encoded</param>
        /// <returns>String converted to Base64</returns>
        public static string EncodeBase64(string text)
        {
            if (text == null)
            {
                return "Cannot Encode Null Value!";
            }

            byte[] textAsBytes = Encoding.UTF8.GetBytes(text);
            return System.Convert.ToBase64String(textAsBytes);
        }

        /// <summary>
        /// Obtains a string from Base64
        /// </summary>
        /// <param name="base64EncodedData">A base 64-encoded String</param>
        /// <returns>A String value that is no longer Encoded</returns>
        public static string DecodeBase64(string base64EncodedData)
        {
            if (base64EncodedData == null)
            {
                return "Cannot Decode Null Value!";
            }

            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
