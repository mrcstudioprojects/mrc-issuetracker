using UnityEngine;

namespace MRC.IssueTracker
{
    public class CardSender : MonoBehaviour
    {
        [HideInInspector] public PlayerLogger playerLogger;

        public BugReportUI bugReportUI;

        private void Awake()
        {
            bugReportUI = FindObjectOfType<BugReportUI>();
            playerLogger = FindObjectOfType<PlayerLogger>();
        }

        public virtual void SendCardToBoard(string title, string description)
        {

        }

        public string GetSystemInformation()
        {
            return "###System Information\n" +
                   "- " + SystemInfo.deviceModel + "\n" +
                   "- " + SystemInfo.operatingSystem + "\n" +
                   "- " + SystemInfo.processorType + "\n" +
                   "- " + SystemInfo.systemMemorySize + " MB\n" +
                   "- " + SystemInfo.graphicsDeviceName + " (" + SystemInfo.graphicsDeviceType + ")\n" +
                   "- " + "Resolution: " + Screen.width + "x" + Screen.height + " at " +
                   Screen.currentResolution.refreshRate + "hz \n" +
                   "- " + "Quality: " + QualitySettings.GetQualityLevel().ToString() + "\n" +
                   "\n" + "___\n";
        }

        public string GetTotalSessionPlaytime()
        {
            return "Total Session Playtime: " + ((Time.realtimeSinceStartup / 3600) % 24).ToString("00") + "h " +
                   Mathf.Floor(Time.realtimeSinceStartup / 60).ToString("00") + "m " +
                   (Time.realtimeSinceStartup % 60).ToString("00") + "s \n";
        }

        public string GetScenePlaytime()
        {
            return "Scene Playtime: " + ((Time.timeSinceLevelLoad / 3600) % 24).ToString("00") + "h " +
                   Mathf.Floor(Time.timeSinceLevelLoad / 60).ToString("00") + "m " +
                   (Time.timeSinceLevelLoad % 60).ToString("00") + "s \n";
        }
    }
}