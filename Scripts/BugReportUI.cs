using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

namespace MRC.IssueTracker
{
    public class BugReportUI : MonoBehaviour
    {
        public UnityEvent OnReporterShow;
        public UnityEvent OnReporterHide;
        public CanvasGroup canvasGroup;
        public GameObject reportUI;
        public GameObject waitUI;
        public InputField title;
        public InputField description;
        public GameObject reportButton;
        public byte[] file = null;
        private bool isOpen = false;
        private bool waitForOpen = false;
        public bool stopTimeOnOpen = true;
        private float prevTimeScale;
        public bool dontDestroy = true;
        
        
        private void Awake()
        {
            if (dontDestroy)
            {
                BugReportUI[] allUIs = FindObjectsOfType<BugReportUI>();
                if (allUIs.Length == 1)
                {
                    DontDestroyOnLoad(gameObject);
                }
                else
                {
                    Destroy(gameObject);
                }
            }

            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            reportUI.SetActive(false);
            waitUI.SetActive(false);
            reportButton.SetActive(true);
        }

        [ContextMenu("Show")]
        public void Show()
        {
            OnReporterShow.Invoke();
            StartCoroutine(ShowBugReporter());
        }

        public void SendCard()
        {
            waitUI.SetActive(true);
            reportUI.SetActive(false);
            FindObjectOfType<CardSender>().SendCardToBoard(title.text, description.text);
        }
        
        private IEnumerator ShowBugReporter()
        {
            waitForOpen = true;
            isOpen = true;
            reportButton.SetActive(false);
            title.text = "";
            description.text = "";
            prevTimeScale = Time.timeScale;
            if (stopTimeOnOpen)
            {
                Time.timeScale = 0;
            }
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            reportUI.SetActive(true);
            waitUI.SetActive(false);
            yield return new WaitForEndOfFrame();
            Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            yield return new WaitForEndOfFrame();
            tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            tex.Apply();
            // Encode texture into JPG
            file = tex.EncodeToJPG();
            Destroy(tex);
            yield return new WaitForEndOfFrame();
            
            canvasGroup.alpha =1;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            waitForOpen = false;
        }

        public void Hide()
        {
            OnReporterHide.Invoke();
            isOpen = false;
            FindObjectOfType<TrackerConfiguration>().bugReportInProgress = false;
            reportUI.SetActive(false);
            waitUI.SetActive(false);
            reportButton.SetActive(true);
            if (stopTimeOnOpen)
            {
                Time.timeScale = prevTimeScale;
            }
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        public void Toggle()
        {
            if (waitForOpen)
            {
                return;
            }
            else
            {
                if (isOpen)
                {
                    Hide();
                }
                else
                {
                    Show();
                }
            }
        }
        
        void Update()
        {
#if ENABLE_INPUT_SYSTEM
            if (Keyboard.current.f2Key.wasPressedThisFrame)
            {
                Toggle();
            }
#else
            if (Input.GetKeyDown(KeyCode.F2))
            {
                Toggle();
            }    
#endif
        }
    }
}
