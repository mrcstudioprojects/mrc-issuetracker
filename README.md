# MRC Issue Tracker

A tool that allows the capture of issue cards within the Unity editor at runtime and automatically uploads them to popular project management softwares such as Trello.

Currently supports Unity and Trello, hoping to support more services (Asana, JIRA, etc) as time goes on. 

# Setup

1. Copy URL `https://gitlab.com/mrcstudioprojects/mrc-issuetracker.git` and add to Unity as a Package

2. Window > Package Manager > + > Add package from git URL > Paste

3. Import Package to your Unity project

4. In Unity Project, Locate `Packages > MRC Issue Tracker > Prefabs > Issue Tracker (Trello)` and make a new prefab variant in your local asset folder. 

5. Select the new prefab variant, and complete the `Trello Auth` section of the `Trello Configuration` component. 

6. You're done! Now during game play, you will be able to click the "BUG" button and log cards from your game. 


# FAQ

Q: Screenshots work fine in Editor but are all black when reported on iOS.

A: The screen can only be captured if `Metal Write-Only Backbuffer` is set to false. This setting can be found in `Project Settings > Player > Other Settings > Metal Write-Only Backbuffer`


Q: When are more API integrations coming?

A: I don't know, but could use your help. This is an open-source project and we would be thrilled to receive contributions. For starters, there is an unfinished `Gitlab Issues.zip` in the project. 